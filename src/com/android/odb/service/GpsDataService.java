package com.android.odb.service;

import java.io.IOException;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.android.odb.utils.LogUtils;
import com.soooner.serialport.SerialPortManager;

public class GpsDataService extends Service {

	private static final String TAG = GpsDataService.class.getSimpleName();

	public IBinder onBind(Intent paramIntent) {
		return null;
	}

	public void onCreate() {
		super.onCreate();
		LogUtils.d(TAG, "----------onCreate----------");
		try {
			SerialPortManager.getInstance().startup();
			return;
		} catch (IOException e) {
			while (true) {
				e.printStackTrace();
				LogUtils.e(TAG, "----SerialPortManager startup Exception!"
						+ e.getMessage());
			}
		}
	}

	public void onDestroy() {
		super.onDestroy();
		LogUtils.d(TAG, "----------onDestroy----------");
		SerialPortManager.getInstance().shutdown();
	}

	public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2) {
		LogUtils.d(TAG, "----------onStartCommand----------");
		try {
			SerialPortManager localSerialPortManager = SerialPortManager
					.getInstance();
			if (localSerialPortManager.isShutDown())
				localSerialPortManager.startup();
			return super.onStartCommand(paramIntent, paramInt1, paramInt2);
		} catch (IOException e) {
			while (true)
				e.printStackTrace();
		}
	}
}