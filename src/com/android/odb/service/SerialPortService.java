package com.android.odb.service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.android.odb.utils.LogUtils;
import com.soooner.serialport.SerialPortManager;

public class SerialPortService extends Service {

	private static final String TAG = SerialPortService.class.getSimpleName();

	public static final String SERIAL_PORT = "/dev/ttyMT1";
	private FileReader mInp;
	private FileWriter mOutp;
	
	
	public IBinder onBind(Intent paramIntent) {
		return null;
	}

	public void onCreate() {
		super.onCreate();
		LogUtils.d(TAG, "----------onCreate----------");
		try {
			SerialPortManager.getInstance().startup();
			return;
		} catch (IOException e) {
			LogUtils.e(
					TAG,
					"----SerialPortManager startup Exception!"
							+ e.getLocalizedMessage());
		}
	}

	public void onDestroy() {
		super.onDestroy();
		LogUtils.d(TAG, "----------onDestroy----------");
		SerialPortManager.getInstance().shutdown();
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		LogUtils.d(TAG, "----------onStartCommand----------");
		try {
			SerialPortManager localSerialPortManager = SerialPortManager
					.getInstance();
			if (localSerialPortManager.isShutDown())
				localSerialPortManager.startup();
			return super.onStartCommand(intent, flags, startId);
		} catch (IOException e) {
			return super.onStartCommand(intent, flags, startId);
		}
	}
	
	public String readSerial() {
		String localObject = "#*#*";
		try {
			char[] arrayOfChar = new char[100];
			int i = this.mInp.read(arrayOfChar);
			if (i > 0) {
				String str = new String(arrayOfChar, 0, i);
				localObject = str;
			}
			return localObject;
		} catch (Exception localException) {
			Log.e(TAG, "read serial file error......");
		}

		return localObject;
	}
	public void initStream() {
		File localFile = new File("/dev/ttyMT1");
		try {
			this.mOutp = new FileWriter(localFile);
			this.mInp = new FileReader(localFile);
			return;
		} catch (Exception localException) {
			while (true)
				Log.e(TAG, "init serial file error......");
		}
	}
	
	public void writeserial(String paramString) {
		try {
			this.mOutp.write(paramString);
			this.mOutp.flush();
			return;
		} catch (Exception localException) {
			while (true) {
				Log.e(TAG, "write serial file error......");
				localException.printStackTrace();
			}
		}
	}
	
	class WriteserialRunnable implements Runnable {

		public void run() {
			SerialPortService.this.writeserial("3g_ready\n");

			try {
				Thread.sleep(3000L);
				SerialPortService.this.writeserial("atinl\n");
			} catch (InterruptedException e) {
				try {
					Thread.sleep(3000L);
					SerialPortService.this.writeserial("atv\r\n");
					Log.e(TAG, "send atv .............");
				} catch (InterruptedException e1) {
					try {
						Thread.sleep(3000L);
					} catch (InterruptedException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
			}
		}
	}

	class ReadSerialRunnable implements Runnable {
		public void run() {
			while (true) {
				String str = SerialPortService.this.readSerial();
				if (!str.equals("#*#*"))
					Log.e(TAG, "read from serial:" + str);
				try {
					Thread.sleep(1000L);
				} catch (Exception localException) {
				}
			}
		}
	}

}
