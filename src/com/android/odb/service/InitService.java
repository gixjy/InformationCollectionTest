package com.android.odb.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.android.odb.utils.LogUtils;
import com.soooner.iov.util.DeviceUtil;

public class InitService extends Service {
	private static final String TAG = InitService.class.getSimpleName();

	public IBinder onBind(Intent paramIntent) {
		return null;
	}

	public void onCreate() {
		super.onCreate();
		LogUtils.d(TAG, "----------onCreate----------");
		new AdDataUnziper().start();
		stopSelf();
	}

	public void onDestroy() {
		super.onDestroy();
		LogUtils.d(TAG, "----------onDestroy----------");
	}

	public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2) {
		LogUtils.d(TAG, "----------onStartCommand----------");
		return super.onStartCommand(paramIntent, paramInt1, paramInt2);
	}

	private class AdDataUnziper extends Thread {
		private AdDataUnziper() {
		}

		public void run() {
			try {
				boolean bool = DeviceUtil.setWifiAP(true);
				LogUtils.d(TAG, "----Start Wifi AP result:" + bool);
				return;
			} catch (Exception e) {
				while (true)
					LogUtils.e(TAG,
							"----Start Wifi AP Exception" + e.getMessage());
			}
		}
	}
}