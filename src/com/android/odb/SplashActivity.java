package com.android.odb;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class SplashActivity extends Activity {
	private static final String TAG = SplashActivity.class.getSimpleName();
	
	public static final String SERIAL_PORT = "/dev/ttyMT1";
	private FileReader mInp;
	private FileWriter mOutp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.e(TAG, "test start------------");
		initStream();
		new Thread(new WriteserialRunnable()).start();
		new Thread(new ReadSerialRunnable()).start();
	}

	public String readSerial() {
		String localObject = "#*#*";
		try {
			char[] arrayOfChar = new char[100];
			int i = this.mInp.read(arrayOfChar);
			if (i > 0) {
				String str = new String(arrayOfChar, 0, i);
				localObject = str;
			}
			return localObject;
		} catch (Exception localException) {
			Log.e(TAG, "read serial file error......");
		}

		return localObject;
	}
	public void initStream() {
		File localFile = new File("/dev/ttyMT1");
		try {
			this.mOutp = new FileWriter(localFile);
			this.mInp = new FileReader(localFile);
			return;
		} catch (Exception localException) {
			while (true)
				Log.e(TAG, "init serial file error......");
		}
	}
	
	public void writeserial(String paramString) {
		try {
			this.mOutp.write(paramString);
			this.mOutp.flush();
			return;
		} catch (Exception localException) {
			while (true) {
				Log.e(TAG, "write serial file error......");
				localException.printStackTrace();
			}
		}
	}
	
	class WriteserialRunnable implements Runnable {

		public void run() {
			SplashActivity.this.writeserial("3g_ready\n");

			try {
				Thread.sleep(3000L);
				SplashActivity.this.writeserial("atinl\n");
			} catch (InterruptedException e) {
				try {
					Thread.sleep(3000L);
					SplashActivity.this.writeserial("atv\r\n");
					Log.e(TAG, "send atv .............");
				} catch (InterruptedException e1) {
					try {
						Thread.sleep(3000L);
					} catch (InterruptedException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
				}
			}
		}
	}

	class ReadSerialRunnable implements Runnable {
		public void run() {
			while (true) {
				String str = SplashActivity.this.readSerial();
				if (!str.equals("#*#*"))
					Log.e(TAG, "read from serial:" + str);
				try {
					Thread.sleep(1000L);
				} catch (Exception localException) {
				}
			}
		}
	}
}
