/**
 *
 *
 * @author 肖俊裕 (harry.xiao@enways.com)
 * @version Revision: 1.0.0 Date: 2014年2月28日
 */
package com.android.odb.utils;

import java.util.Set;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * 
 * 
 * @author 肖俊裕 (harry.xiao@enways.com)
 * @version Revision: 1.0.0 Date: 2014年2月28日
 */
@SuppressLint("NewApi")
public class AxiPreference {

	private SharedPreferences preferences;

	public AxiPreference(Context context) {
		this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
	}

	public void removeByKey(String key) {
		SharedPreferences.Editor localEditor = this.preferences.edit();
		localEditor.remove(key);
		localEditor.commit();
	}

	public void saveBoolean(String key, boolean value) {
		SharedPreferences.Editor localEditor = this.preferences.edit();
		localEditor.putBoolean(key, value);
		localEditor.commit();
	}

	public boolean getBoolean(String key, boolean defaultValue) {
		return this.preferences.getBoolean(key, defaultValue);
	}

	public void saveInt(String key, int value) {
		SharedPreferences.Editor localEditor = this.preferences.edit();
		localEditor.putInt(key, value);
		localEditor.commit();
	}

	public int getInt(String key, int defaultValue) {
		return this.preferences.getInt(key, defaultValue);
	}

	public void saveLong(String key, long value) {
		SharedPreferences.Editor localEditor = this.preferences.edit();
		localEditor.putLong(key, value);
		localEditor.commit();
	}

	public long getLong(String key, long defaultValue) {
		return this.preferences.getLong(key, defaultValue);
	}

	public void saveFloat(String key, float value) {
		SharedPreferences.Editor localEditor = this.preferences.edit();
		localEditor.putFloat(key, value);
		localEditor.commit();
	}

	public float getFloat(String key, float defaultValue) {
		return this.preferences.getFloat(key, defaultValue);
	}

	public void saveString(String key, String value) {
		SharedPreferences.Editor localEditor = this.preferences.edit();
		localEditor.putString(key, value);
		localEditor.commit();
	}

	public String getString(String key, String defaultValue) {
		return this.preferences.getString(key, defaultValue);
	}

	public void saveStringSet(String key, Set<String> value) {
		SharedPreferences.Editor editor = this.preferences.edit();
		editor.putStringSet(key, value);
		editor.commit();
	}

	@SuppressLint("NewApi")
	public Set<String> getStringSet(String key, Set<String> defaultValue) {
		return this.preferences.getStringSet(key, defaultValue);
	}

	public String getAutoCode() {
		return getString(AUTO_CODE_KEY, null);
	}

	private static final String AUTO_CODE_KEY = "auto_code_key";
	public void setAutoCode(String autoCode) {
		saveString(AUTO_CODE_KEY, autoCode);
	}
	

	private static final String TRAVEL_ID_KEY = "travel_id_key";
	

	public String getTravelId() {
		return getString(TRAVEL_ID_KEY, null);
	}

	public void setTravelId(String travelId) {
		saveString(TRAVEL_ID_KEY, travelId);
	}
}
