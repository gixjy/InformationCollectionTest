package com.android.odb.utils;

import android.util.Log;

import com.android.odb.BuildConfig;

public class LogUtils {

	public static void d(String tag, String msg) {
		if (BuildConfig.DEBUG) {
			Log.d(tag, msg);
		}
	}
	
	public static void e(String tag, String msg) {
		if (BuildConfig.DEBUG) {
			Log.e(tag, msg);
		}
	}
}
