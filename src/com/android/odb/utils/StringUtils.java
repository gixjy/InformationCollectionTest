/**
 * 
 */
package com.android.odb.utils;

import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Administrator
 * 
 */
public class StringUtils {
	
	public static boolean isEmpty(String value) {
		return (value == null) || (value.length() == 0);
	}

	public static boolean isNotEmpty(String value) {
		return !isEmpty(value);
	}

	/**
     * 描述：将null转化为“”.
     *
     * @param str 指定的字符串
     * @return 字符串的String类型
     */
    public static String parseEmpty(String str) {
        if(str == null || "null".equals(str.trim())){
        	str = "";
        }
        return str.trim();
    }
    
	public static String buildQueryString(String url,
			Map<String, String> params, boolean encode) {
		try {
			if (params != null) {
				StringBuffer requestParam = new StringBuffer();
				Iterator<String> iterator = params.keySet().iterator();
				while (iterator.hasNext()) {
					String str = (String) iterator.next();
					if (requestParam.length() > 0) {
						requestParam.append("&");
					}
						
					if (encode) {
						requestParam.append(str + "=" + URLEncoder.encode((String) params.get(str), "utf-8"));
					} else {
						requestParam.append(str + "=" + (String) params.get(str));
					}
				}
				
				if (requestParam.length() > 0) {
					url = url + "?";
					url = url + requestParam.toString();
				}
				
			}
		} catch (Exception localException) {
		}
		
		return url;
	}

	public static String getByteString(int bytes) {
		double d1 = bytes / 1024.0D;
		if (d1 > 1024.0D) {
			double d2 = d1 / 1024.0D;
			if (d2 > 1024.0D) {
				double d3 = d2 / 1024.0D;
				return String.format("%.2fG",
						new Object[] { Double.valueOf(d3) });
			}
			return String.format("%.2fM", new Object[] { Double.valueOf(d2) });
		}
		return String.format("%.0fk", new Object[] { Double.valueOf(d1) });
	}

	public static String getByteString(long bytes) {
		double d1 = bytes / 1024.0D;
		if (d1 > 1024.0D) {
			double d2 = d1 / 1024.0D;
			if (d2 > 1024.0D) {
				double d3 = d2 / 1024.0D;
				return String.format("%.2fG",
						new Object[] { Double.valueOf(d3) });
			}
			
			return String.format("%.2fM", new Object[] { Double.valueOf(d2) });
		}
		
		return String.format("%.0fk", new Object[] { Double.valueOf(d1) });
	}
	
	public static boolean isValid(String msg) {
		return true;
	}
}
